package br.com.ozeano.curso.api.bb.domain.service.file;

import br.com.caelum.stella.boleto.Datas;
import br.com.ozeano.curso.api.bb.domain.model.Fatura;
import br.com.ozeano.curso.api.bb.infra.model.input.CobrancaInput;

public interface GeradorDeBoletoService {

	public byte[] gerar(Fatura fatura, CobrancaInput cobranca);
	
	
	default Datas criarDatas(Fatura fatura) {
		
		var vencimento = fatura.getDataVencimento();
		var criadoEm = fatura.getCriadoEm();
		var atualizadoEm = fatura.getAtualizadoEm();
		
		var datas = Datas.novasDatas()
				.comDocumento(criadoEm.getDayOfMonth(), criadoEm.getMonthValue(), criadoEm.getYear())
				.comProcessamento(atualizadoEm.getDayOfMonth(), atualizadoEm.getMonthValue(), atualizadoEm.getYear())
				.comVencimento(vencimento.getDayOfMonth(), vencimento.getMonthValue(), vencimento.getYear());
		
		return datas;
	}
	
}
